package com.example.maxvek.myapplication;

import android.database.Cursor;
import android.util.Log;
import com.example.maxvek.myapplication.repository.DatabaseInstance;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.util.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        JSONArray resultSet = new JSONArray();
        Cursor cursor = DatabaseInstance.getInstance().getMyDB().getAllForFile();
        if (cursor.moveToFirst()){
            while (cursor.moveToNext()) {
                int totalColumn = cursor.getColumnCount();
                JSONObject rowObject = new JSONObject();
                for (int i = 0; i < totalColumn; i++){
                    if( cursor.getColumnName(i) != null )
                    {
                        try
                        {
                            if( cursor.getString(i) != null )
                            {
                                Log.d("TAG_NAME", cursor.getString(i) );
                                rowObject.put(cursor.getColumnName(i) ,  cursor.getString(i) );
                            }
                            else
                            {
                                rowObject.put( cursor.getColumnName(i) ,  "" );
                            }
                        }
                        catch( Exception e )
                        {
                            Log.d("TAG_NAME", e.getMessage()  );
                        }
                    }
                    resultSet.put(rowObject);
                }
                cursor.close();
                Log.d("TAG_NAME", resultSet.toString() );
            }
        }

    }
}