package com.example.maxvek.myapplication.activity;

import java.util.ArrayList;

public interface UpdateText {
    void updateText(ArrayList<String> text);
}
