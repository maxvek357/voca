package com.example.maxvek.myapplication.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import com.example.maxvek.myapplication.R;
import com.example.maxvek.myapplication.fragments.*;
import com.example.maxvek.myapplication.repository.DatabaseInstance;
import com.example.maxvek.myapplication.utils.LocaleHelper;

import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DatabaseInstance.getInstance().init(this);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        ((NavigationView) findViewById(R.id.nav_view)).setNavigationItemSelectedListener(this);

        ViewGroup mainLayout = findViewById(R.id.include_indrawer);
        mainLayout.removeAllViews();
        Fragment fragment = new ContextMain();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.include_indrawer, fragment).commit();



        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken


//        convert
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, getResources().getString(R.string.ma_mail), Snackbar.LENGTH_LONG)
                        .setAction(getResources().getString(R.string.ma_mail_button), new MyMailListener()).show();
            }
        });
    }

    public class MyMailListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + getResources().getString(R.string.ma_mail_mail)));
            intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.ma_subject));
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.toolbar_info) {
//
//// inflate the layout of the popup window
//            LayoutInflater inflater = (LayoutInflater)
//                    getSystemService(LAYOUT_INFLATER_SERVICE);
//            View popupView = inflater.inflate(R.layout.popup_window, null);
//
//            // create the popup window
//            int width = LinearLayout.LayoutParams.WRAP_CONTENT;
//            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
//            final PopupWindow popupWindow = new PopupWindow(popupView, width, height,true);
//            popupWindow.showAtLocation(findViewById(R.id.include_indrawer), Gravity.CENTER, 0, 0);
//            popupView.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    popupWindow.dismiss();
//                    return true;
//                }
//            });
//
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        ViewGroup mainLayout = findViewById(R.id.include_indrawer);
        mainLayout.removeAllViews();

        switch (id) {
            case R.id.nav_translate:
                changeFragment(new ContextMain());
                break;
            case R.id.nav_voca:
                changeFragment(new VocabularyFragment());
                break;
            case R.id.nav_training:
                changeFragment(new SelectTraining());
                break;
            case R.id.nav_setting:
                changeFragment(new SettingFragment());
                break;
            case R.id.nav_statistic:
                changeFragment(new StatisticFragment());
                break;
            case R.id.nav_share:
                changeFragment(new ExportFragment());
                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void changeFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.include_indrawer, fragment).commit();
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }
}
