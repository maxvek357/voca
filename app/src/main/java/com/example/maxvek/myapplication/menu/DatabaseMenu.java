package com.example.maxvek.myapplication.menu;

import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.example.maxvek.myapplication.repository.DatabaseInstance;
import com.example.maxvek.myapplication.R;

public class DatabaseMenu implements PopupMenu.OnMenuItemClickListener {
    private View view;
    private String from;

    public DatabaseMenu(View view, String from) {
        this.view = view;
        this.from = from;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_action_menu:
                TableLayout parent = (TableLayout) view.getParent();
                TableRow t = (TableRow) view;
                TextView firstTextView = (TextView) t.getChildAt(0);
                String firstText;
                if (firstTextView.getText() != null) {
                    firstText = firstTextView.getText().toString();
                    DatabaseInstance.getInstance().getMyDB().deleteData(from, firstText);
                }
                parent.removeView(view);
                return true;
        }
        return false;
    }
}
