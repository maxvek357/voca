package com.example.maxvek.myapplication.utils;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.maxvek.myapplication.R;

import java.util.ArrayList;
import java.util.Locale;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private ArrayList<String> arrayList = new ArrayList<>();
    private Context mContext;
    private String viewLayout, lang;
    private TextToSpeech mTTS;

    public RecyclerViewAdapter(Context context, ArrayList<String> list, String viewLayout) {
        this.arrayList = list;
        this.mContext = context;
        this.viewLayout = viewLayout;
    }
    public RecyclerViewAdapter(Context context, ArrayList<String> list, String viewLayout, String lang) {
        this.arrayList = list;
        this.mContext = context;
        this.viewLayout = viewLayout;
        this.lang = lang;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem, parent, false);
        if (viewLayout.equals("ContextMain")) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.translate_card_list, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.textView.setText(arrayList.get(position));
        if ((viewLayout.equals("ContextMain"))&&(lang.equals("en")||lang.equals("de")||lang.equals("fr"))) {
            holder.imageView.setVisibility(View.VISIBLE);
            mTTS = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        Locale langtts = Locale.ENGLISH;
                        switch (lang){
                            case "de":
                                langtts = Locale.GERMAN;
                                break;
                            case "fr":
                                langtts = Locale.FRANCE;
                                break;
                        }
                        int result = mTTS.setLanguage(langtts);
                        if (result == TextToSpeech.LANG_MISSING_DATA
                                || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                            Log.e("TTS", "Language not supported");
                        }
                    } else {
                        Log.e("TTS", "Initialization failed");
                    }
                }
            });
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    speak(arrayList.get(position));
                }
            });
        }
    }
    @SuppressWarnings("deprecation")
    private void speak(String text) {
        /*float pitch = (float) mSeekBarPitch.getProgress() / 50;
        if (pitch < 0.1) pitch = 0.1f;
        float speed = (float) mSeekBarSpeed.getProgress() / 50;
        if (speed < 0.1) speed = 0.1f;

        mTTS.setPitch(pitch);
        mTTS.setSpeechRate(speed);*/
        mTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            if (viewLayout.equals("ContextMain")) {
                textView = itemView.findViewById(R.id.translate_tw_cw);
                imageView = itemView.findViewById(R.id.translate_tts_iv);
            } else {
                textView = itemView.findViewById(R.id.train3_text_view);
            }
        }
    }
}
