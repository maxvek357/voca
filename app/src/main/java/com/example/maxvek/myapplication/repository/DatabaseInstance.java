package com.example.maxvek.myapplication.repository;

import android.content.Context;
import com.example.maxvek.myapplication.utils.DatabaseHelper;

public class DatabaseInstance {
    private static DatabaseInstance ourInstance = new DatabaseInstance();
    private DatabaseHelper myDB;

    private DatabaseInstance() {
    }

    public static DatabaseInstance getInstance() {
        return ourInstance;
    }

    public synchronized void init(Context context) {
        if (myDB == null) {
            myDB = new DatabaseHelper(context);
        }
    }

    public DatabaseHelper getMyDB() {
        return myDB;
    }
}
