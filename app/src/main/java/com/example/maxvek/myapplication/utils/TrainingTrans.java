package com.example.maxvek.myapplication.utils;

import com.example.maxvek.myapplication.repository.DatabaseInstance;

import java.util.*;

public class TrainingTrans {


    public static List<Quiz> getWords(String from, String to, int difficult) {
        Map<String, List<String>> hmapVoc = DatabaseInstance.getInstance().getMyDB().getAllForVocabulary(from, to);
        int difficultType, difficultLevel;
        switch (difficult) {
            case 0:
                difficultType = 3;
                difficultLevel = 1;
                break;
            case 1:
                difficultType = 5;
                difficultLevel = 1;
                break;
            case 2:
                difficultType = 7;
                difficultLevel = 2;
                break;
            default:
                difficultType = 3;
                difficultLevel = 1;
                break;
        }

        Random random = new Random();

        Iterator<String> iterator = hmapVoc.keySet().iterator();
        List<Quiz> quizzes = new ArrayList<>();
        while (iterator.hasNext()) {
            String key = iterator.next();
            List<String> trueAnswer = hmapVoc.get(key);
            Quiz quiz = new Quiz();
            quiz.question = key;
            if (difficultLevel == 1){
                ArrayList<String> listCorr = new ArrayList<>(1);
                listCorr.add(trueAnswer.get(random.nextInt(trueAnswer.size())));
                quiz.correctAnswer = listCorr;
            }
            else {
                quiz.correctAnswer = trueAnswer;
            }

            Set<String> list = new HashSet<>();
            Set<Map.Entry<String, List<String>>> entries = hmapVoc.entrySet();
            for (Map.Entry<String, List<String>> entry : entries) {
                if (entry.getKey().equals(key)) {
                    continue;
                }
                list.add(entry.getValue().get(random.nextInt(entry.getValue().size())));
                if (list.size() == difficultType) {
                    break;
                }
            }
            quiz.incorrectAnswer = new ArrayList<>(list);
            quizzes.add(quiz);

            if (quizzes.size() == 20) {
                break;
            }
        }
        return quizzes;

    }

    public static List<Quiz> getWords(String from, String to){
        Map<String, List<String>> hmapVoc = DatabaseInstance.getInstance().getMyDB().getAllForVocabulary(from, to);
        Iterator<String> iterator = hmapVoc.keySet().iterator();
        List<Quiz> quizzes = new ArrayList<>();
        while (iterator.hasNext()) {
            String key = iterator.next();
            List<String> trueAnswer = hmapVoc.get(key);
            Quiz quiz = new Quiz();
            quiz.question = key;
            quiz.correctAnswer = trueAnswer;
            quizzes.add(quiz);
        }
        return quizzes;
    }

    public static class Quiz {
        String question;
        List<String> correctAnswer;
        List<String> incorrectAnswer;

        @Override
        public String toString() {
            return "Quiz{" +
                    "question = " + question + '\'' +
                    "correctAnswer='" + Arrays.toString(correctAnswer.toArray()) + '\'' +
                    ", BadAnswers=" + Arrays.toString(incorrectAnswer.toArray()) +
                    '}';
        }

        public String getQuestion() {
            return this.question;
        }

        public List<String> getCorrectAnswer() {
            return this.correctAnswer;
        }

        public List<String> getIncorrectAnswer() {
            return this.incorrectAnswer;
        }

        public int getCorrectCount(){
            return correctAnswer.size();
        }

        public int getIncorrectCount(){
            return incorrectAnswer.size();
        }

    }

}
