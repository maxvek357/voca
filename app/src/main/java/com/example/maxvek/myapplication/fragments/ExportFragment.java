package com.example.maxvek.myapplication.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.maxvek.myapplication.R;
import com.example.maxvek.myapplication.repository.DatabaseInstance;
import com.example.maxvek.myapplication.utils.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.*;

import static android.app.Activity.RESULT_OK;
import static com.example.maxvek.myapplication.fragments.ContextMain.initSpinnerFrom;
import static com.example.maxvek.myapplication.fragments.ContextMain.initSpinnerTo;

public class ExportFragment extends Fragment {
    private Spinner fromSpinner, toSpinner;
    private Button showAll, showPart, expAll, expPart, exportBut, importBut;
    private TextView testTW;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.export_fragment, container, false);
        fromSpinner = rootView.findViewById(R.id.from_lang_spinner);
        toSpinner = rootView.findViewById(R.id.to_lang_spinner);
        showAll = rootView.findViewById(R.id.export_all_show);
        showPart = rootView.findViewById(R.id.export_part_show);
        expAll = rootView.findViewById(R.id.export_all);
        expPart = rootView.findViewById(R.id.export_part);
        exportBut = rootView.findViewById(R.id.export_but);
        importBut = rootView.findViewById(R.id.import_but);
        testTW = rootView.findViewById(R.id.testTW);
        final LinearLayout llmain = rootView.findViewById(R.id.export_ll_main);
        final LinearLayout llall = rootView.findViewById(R.id.export_ll_all);
        final LinearLayout llpart = rootView.findViewById(R.id.export_ll_part);
        initSpinnerTo(rootView);
        initSpinnerFrom(rootView);

        expAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FileUtils.exportFile();
                try {
                    FileUtils.copyDB(Objects.requireNonNull(getContext()).getDatabasePath("vocabulary.db"), Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_DOCUMENTS) + "dictionary" +"/vocabulary.db");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        expPart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        exportBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llmain.setVisibility(View.VISIBLE);
                llall.setVisibility(View.VISIBLE);
                llpart.setVisibility(View.GONE);
            }
        });

        importBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llmain.setVisibility(View.GONE);
                llall.setVisibility(View.GONE);
                llpart.setVisibility(View.GONE);
                Intent intent = new Intent()
                        .setType("*/*")
                        .setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select a file"), 123);


            }
        });

        showAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llall.setVisibility(View.VISIBLE);
                llpart.setVisibility(View.GONE);
            }
        });

        showPart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llpart.setVisibility(View.VISIBLE);
                llall.setVisibility(View.GONE);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==123 && resultCode==RESULT_OK) {
            Uri selectedfile = data.getData();
            try {
                FileUtils.copyDB(Objects.requireNonNull(getContext()).getDatabasePath("vocabulary.db"), getContext().getFilesDir()+"/vocabulary.db");
                DatabaseInstance.getInstance().getMyDB().importDatabase(selectedfile.getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
