package com.example.maxvek.myapplication.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.maxvek.myapplication.R;
import com.example.maxvek.myapplication.repository.DatabaseInstance;
import com.example.maxvek.myapplication.utils.SwipeRecyclerViewAdapter;

import java.util.*;

import static android.app.Activity.RESULT_OK;
import static com.example.maxvek.myapplication.fragments.ContextMain.initSpinnerFrom;
import static com.example.maxvek.myapplication.fragments.ContextMain.initSpinnerTo;

public class VocabularyFragment extends Fragment {

    private final static ArrayList languageType = new ArrayList();
    private Spinner fromSpinner, toSpinner;
    private RecyclerView recyclerView;
    private AutoCompleteTextView  autoText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_vocabulary, container, false);
        fromSpinner = rootView.findViewById(R.id.from_lang_spinner);
        toSpinner = rootView.findViewById(R.id.to_lang_spinner);
        recyclerView = rootView.findViewById(R.id.recycler_view_voca);
        autoText = rootView.findViewById(R.id.show_voca_edit_text);
        final LinearLayout linearLayout = rootView.findViewById(R.id.show_voc_ll);
        languageType.addAll(DatabaseInstance.getInstance().getMyDB().getAllColum());
        rootView.findViewById(R.id.show_voc_but).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String from = languageType.get(fromSpinner.getSelectedItemPosition()).toString();
                String to = languageType.get(toSpinner.getSelectedItemPosition()).toString();
                Map<String, List<String>> hmapVoc = DatabaseInstance.getInstance().getMyDB().getAllForVocabulary(from, to);
                if (hmapVoc.size() != 0) {
                    showVocabulary(from, to, hmapVoc);
                    linearLayout.setVisibility(View.VISIBLE);
                }
                else {
                    recyclerView.removeAllViewsInLayout();
                    linearLayout.setVisibility(View.GONE);
                }
            }
        });
        initSpinnerTo(rootView);
        initSpinnerFrom(rootView);

        rootView.findViewById(R.id.show_voca_hint_but).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String word = autoText.getText().toString();
                String lang = fromSpinner.getSelectedItem().toString();
                String toLang = toSpinner.getSelectedItem().toString();
                Cursor cursor = DatabaseInstance.getInstance().getMyDB().getWordData(lang, word);
                if (cursor.moveToFirst()) {
                    Map<String, List<String>> hmapVoc = new HashMap<>();
                    ArrayList<String> values = new ArrayList<>();
                    values.add(cursor.getString(cursor.getColumnIndex(toLang)));
                    hmapVoc.put(cursor.getString(cursor.getColumnIndex(lang)), values);
                    if (hmapVoc.size() != 0) {
                        showVocabulary(lang, toLang, hmapVoc);
                    }
                }
            }
        });

        autoText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                showHintText(autoText, fromSpinner, Objects.requireNonNull(getContext()));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        rootView.findViewById(R.id.image_view_stt_voca).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, Locale.getDefault());

                if (intent.resolveActivity(Objects.requireNonNull(getActivity()).getPackageManager()) != null) {
                    startActivityForResult(intent, 10);
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.speech_to_text), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    autoText.setText(result.get(0));
                }
                break;
        }
    }

    public void showHintText(AutoCompleteTextView autoText, Spinner fromSpinner, @NonNull Context context) {
        String lang = fromSpinner.getSelectedItem().toString();
        String word = autoText.getText().toString();
        List<String> list = new ArrayList<>(DatabaseInstance.getInstance().getMyDB().getHint(word, lang));
        if (list.size() != 0) {
            autoText.setAdapter(new ArrayAdapter<>(context, R.layout.hint_list, list));
        }
    }

    private void showVocabulary(final String from, String to, Map<String, List<String>> hmapVoc) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView.Adapter mAdapter = new SwipeRecyclerViewAdapter(getContext(), hmapVoc, from, to);
        recyclerView.setAdapter(mAdapter);
    }
}
