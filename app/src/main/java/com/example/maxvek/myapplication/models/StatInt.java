package com.example.maxvek.myapplication.models;

public class StatInt {
    public int cor;
    public int incor;
    public String word;

    public int getCor() {
        return this.cor;
    }

    public int getIncor() {
        return this.incor;
    }

    public String getWord() {
        return this.word;
    }

    public int getCount(){
        return this.cor+this.incor;
    }

    @Override
    public String toString() {
        return "StatInt{" +
                "cor = " + cor + " incor = " + incor + " word = " + word + "}";
    }
}
