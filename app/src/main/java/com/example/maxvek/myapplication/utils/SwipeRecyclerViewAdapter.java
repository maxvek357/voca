package com.example.maxvek.myapplication.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.example.maxvek.myapplication.R;
import com.example.maxvek.myapplication.repository.DatabaseInstance;

import java.util.*;

public class SwipeRecyclerViewAdapter extends RecyclerSwipeAdapter<SwipeRecyclerViewAdapter.SimpleViewHolder> {

    private Context mContext;
    private List<String> listWords;
    private List<String> listTranslate;
    private String from, to;
    private TextToSpeech mTTS;

    public SwipeRecyclerViewAdapter(Context context, Map<String, List<String>> objects, String from, String to) {
        List<String> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();
        if (!objects.isEmpty()) {
            for (String s : objects.keySet()) {
                StringBuffer sb = new StringBuffer();
                sb.append(TextUtils.join(" ", objects.get(s)));
                list1.add(s);
                list2.add(sb.toString());
            }
        }
        this.mContext = context;
        this.listWords = list1;
        this.listTranslate = list2;
        this.from = from;
        this.to = to;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.swipe_layout, parent, false);
        return new SimpleViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder viewHolder, @SuppressLint("RecyclerView") final int position) {
        final String word = listWords.get(position);
        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        viewHolder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
            }
        });
        viewHolder.buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemManger.removeShownLayouts(viewHolder.swipeLayout);
                listWords.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, listWords.size());
                mItemManger.closeAllItems();
                DatabaseInstance.getInstance().getMyDB().deleteData(from, word);
                Toast.makeText(view.getContext(), "Deleted " + viewHolder.textViewWord.getText().toString() + "!", Toast.LENGTH_SHORT).show();
            }
        });
        viewHolder.textViewPos.setText((position + 1) + ".");
        viewHolder.textViewWord.setText(word);
        viewHolder.textViewTranslate.setText(listTranslate.get(position));
        if ((to.equals("en")||to.equals("de")||to.equals("fr"))) {
            viewHolder.imageView.setVisibility(View.VISIBLE);
            mTTS = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        Locale langtts = Locale.ENGLISH;
                        switch (to){
                            case "de":
                                langtts = Locale.GERMAN;
                                break;
                            case "fr":
                                langtts = Locale.FRANCE;
                                break;
                        }
                        int result = mTTS.setLanguage(langtts);
                        if (result == TextToSpeech.LANG_MISSING_DATA
                                || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                            Log.e("TTS", "Language not supported");
                        }
                    } else {
                        Log.e("TTS", "Initialization failed");
                    }
                }
            });
            viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    StringBuilder sb = new StringBuilder();
//                    for (String aListTranslate : listTranslate.get(position)) {
//                        sb.append(aListTranslate).append(" ");
//                    }
                    speak(listTranslate.get(position));
                }
            });
        }
    }

    @SuppressWarnings("deprecation")
    private void speak(String text) {
        mTTS.setSpeechRate(0.3f);
        mTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public int getItemCount() {
        return listWords.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_layout;
    }

    static class SimpleViewHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        TextView textViewPos, textViewWord, textViewTranslate;
        Button buttonDelete;
        ImageView imageView;

        SimpleViewHolder(final View itemView) {
            super(itemView);
            swipeLayout = itemView.findViewById(R.id.swipe_layout);
            textViewPos = itemView.findViewById(R.id.position_sw_layout);
            textViewWord = itemView.findViewById(R.id.word_sw_layout);
            textViewTranslate = itemView.findViewById(R.id.translate_sw_layout);
            buttonDelete = itemView.findViewById(R.id.delete_sw_layout);
            imageView = itemView.findViewById(R.id.voca_tts_iv);
        }
    }
}
