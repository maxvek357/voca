package com.example.maxvek.myapplication.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.maxvek.myapplication.R;
import com.example.maxvek.myapplication.activity.UpdateText;
import com.example.maxvek.myapplication.models.TranslateModel;
import com.example.maxvek.myapplication.repository.DatabaseInstance;
import com.example.maxvek.myapplication.utils.RecyclerViewAdapter;
import com.example.maxvek.myapplication.utils.RequestPool;
import com.google.gson.Gson;
import org.json.JSONObject;

import java.io.*;
import java.net.URLEncoder;
import java.util.*;

import static android.app.Activity.RESULT_OK;

public class ContextMain extends Fragment {
    private final static ArrayList languageType = new ArrayList();
    private EditText editText;
    private Spinner fromSpinner, toSpinner;
    private RecyclerView mRecyclerView;
    private ArrayList<String> translatedWords = new ArrayList<>();
    private Button addToBdBut;
    private UpdateText updateTextCallback = new UpdateText() {
        @Override
        public void updateText(ArrayList<String> text) {
            setRecyclerView(mRecyclerView, text);
        }
    };

    public static void initSpinnerTo(@NonNull final View view) {
        final ArrayList<String> list = new ArrayList<>(DatabaseInstance.getInstance().getMyDB().getAllColum());
        ArrayAdapter<String> adapterFrom = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_spinner_item, list);
        adapterFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = view.findViewById(R.id.to_lang_spinner);
        spinner.setAdapter(adapterFrom);
        spinner.setSelection(1);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v,
                                       int position, long id) {
                Spinner spinner = view.findViewById(R.id.from_lang_spinner);
                int spinnerTo = spinner.getSelectedItemPosition();
                if (position == spinnerTo) {
                    if (list.size() - 1 == position) {
                        spinner.setSelection(position - 1);
                    } else {
                        spinner.setSelection(position + 1);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    public static void initSpinnerFrom(@NonNull final View view) {
        final ArrayList<String> list = new ArrayList<>(DatabaseInstance.getInstance().getMyDB().getAllColum());
        ArrayAdapter<String> adapterFrom = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_spinner_item, list);
        adapterFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = view.findViewById(R.id.from_lang_spinner);
        spinner.setAdapter(adapterFrom);
        spinner.setSelection(0);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v,
                                       int position, long id) {
                Spinner spinner = view.findViewById(R.id.to_lang_spinner);
                int spinnerTo = spinner.getSelectedItemPosition();
                if (position == spinnerTo) {
                    if (list.size() - 1 == position) {
                        spinner.setSelection(position - 1);
                    } else {
                        spinner.setSelection(position + 1);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //после голосового ввода
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    editText.setText(result.get(0));
                }
                break;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.content_main2, container, false);
        editText = rootView.findViewById(R.id.editText);
        fromSpinner = rootView.findViewById(R.id.from_lang_spinner);
        toSpinner = rootView.findViewById(R.id.to_lang_spinner);
        mRecyclerView = rootView.findViewById(R.id.context_main_recyclerView);
        addToBdBut = rootView.findViewById(R.id.addtobd_but);

        languageType.addAll(DatabaseInstance.getInstance().getMyDB().getAllColum());
        rootView.findViewById(R.id.translate_but).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String word = editText.getText().toString().trim();
                if (!word.isEmpty()) {
                    String from = languageType.get(fromSpinner.getSelectedItemPosition()).toString();
                    String to = languageType.get(toSpinner.getSelectedItemPosition()).toString();
                    initForTranslate(from, to, word, updateTextCallback);
                }
            }
        });
        addToBdBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String from = languageType.get(fromSpinner.getSelectedItemPosition()).toString();
                String dest = languageType.get(toSpinner.getSelectedItemPosition()).toString();
                for (int i = 0; i < translatedWords.size(); i++) {
                    boolean isInserted = DatabaseInstance.getInstance().getMyDB().insertData(from, dest, editText.getText().toString().trim(), translatedWords.get(i));
                    if (isInserted)
                        Toast.makeText(getContext(), "Data Inserted", Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(getContext(), "Data not Inserted", Toast.LENGTH_LONG).show();
                }
                translatedWords.clear();
                addToBdBut.setVisibility(View.GONE);
            }
        });
        (rootView.findViewById(R.id.image_view_stt)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, Locale.getDefault());

                if (intent.resolveActivity(Objects.requireNonNull(getActivity()).getPackageManager()) != null) {
                    startActivityForResult(intent, 10);
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.speech_to_text), Toast.LENGTH_SHORT).show();
                }
            }
        });
        initSpinnerTo(rootView);
        initSpinnerFrom(rootView);
        return rootView;
    }

    private void setRecyclerView(RecyclerView recyclerView, ArrayList<String> dataList) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        final RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), dataList, "ContextMain", languageType.get(toSpinner.getSelectedItemPosition()).toString());
        recyclerView.setAdapter(adapter);
    }

    private void initForTranslate(String from, String to, String word, final UpdateText callback) {
        Cursor res = DatabaseInstance.getInstance().getMyDB().getWordData(from, word);
        translatedWords.clear();
        final ArrayList<String> list = new ArrayList<>();
        if (res.getCount() == 0) {
            String url = null;
            try {
                url = "https://glosbe.com/gapi/translate?from=" + from + "&dest=" + to + "&format=json&pretty=true&phrase=" + URLEncoder.encode(word, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Gson gson = new Gson();
                            TranslateModel translateModel = gson.fromJson(response.toString(), TranslateModel.class);
                            for (int i = 0; i < translateModel.tuc.length && i < 5; i++) {
                                if (translateModel.tuc[i].phrase != null && translateModel.tuc[i].phrase.text != null) {
                                    list.add(translateModel.tuc[i].phrase.text);
                                    translatedWords.add(translateModel.tuc[i].phrase.text);
                                    callback.updateText(list);
                                    addToBdBut.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getContext(), "WRONG", Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        }
                    }
            );
            RequestPool.getInstance(getContext()).addToRequestque(jsonObjectRequest);
        } else {
            while (res.moveToNext()) {
                list.add(res.getString(res.getColumnIndex(to)));
            }
            addToBdBut.setVisibility(View.GONE);
        }
        callback.updateText(list);
    }


}
