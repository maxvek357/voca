package com.example.maxvek.myapplication.utils;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class RequestPool {
    private static RequestPool mInstance;
    private static Context mCtx;
    private RequestQueue requestQueue;

    private RequestPool(Context context) {
        mCtx = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized RequestPool getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new RequestPool(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestque(Request<T> request) {
        requestQueue.add(request);
    }
}
