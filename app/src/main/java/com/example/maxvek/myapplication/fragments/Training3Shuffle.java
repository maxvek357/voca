package com.example.maxvek.myapplication.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.maxvek.myapplication.R;
import com.example.maxvek.myapplication.utils.TrainingTrans;
import com.example.maxvek.myapplication.utils.RecyclerViewAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Training3Shuffle extends Fragment {
    private String fromSpinner, toSpinner, curQuestString;
    private int difficult, currentQuestion = 0;
    private TextView variantTV, CorrectCount, IncorrectCount, timeLeftCount;
    private RecyclerView mRecyclerView;
    private ArrayList<String> charQuestionList = new ArrayList<>();
    private ArrayList<String> correctArrayList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.training_3_shuffle, container, false);
        final Bundle bundle = this.getArguments();
        if (bundle != null) {
            fromSpinner = bundle.getString("fromSpinner");
            toSpinner = bundle.getString("toSpinner");
            difficult = bundle.getInt("difficult");
        }
        CorrectCount = rootView.findViewById(R.id.correct_count_tview);
        IncorrectCount = rootView.findViewById(R.id.incorrect_count_tview);
        variantTV = rootView.findViewById(R.id.train3_variant_tv);
        timeLeftCount = rootView.findViewById(R.id.time_left_count);
        mRecyclerView = rootView.findViewById(R.id.train3_recyclerView);
        final Button nextButton = rootView.findViewById(R.id.next_train_shuffle);
        final Button submButton = rootView.findViewById(R.id.subm_train_shuffle);

        final Training1Translate training1Translate = new Training1Translate();
        training1Translate.setDiffText((TextView) rootView.findViewById(R.id.difficult_type));

        final List<TrainingTrans.Quiz> quizzes = TrainingTrans.getWords(fromSpinner, toSpinner);
        Collections.shuffle(quizzes);

        setQuestion(quizzes.get(currentQuestion), difficult);
        if (difficult == 2) {
            (rootView.findViewById(R.id.time_left_text)).setVisibility(View.VISIBLE);
            timeLeftCount.setVisibility(View.VISIBLE);
            training1Translate.startTimer(timeLeftCount, submButton, quizzes.get(currentQuestion).getCorrectCount() + 45);
        }

        submButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer();
                currentQuestion++;
                nextButton.setVisibility(View.VISIBLE);
                submButton.setVisibility(View.GONE);
                if (difficult == 2) training1Translate.stopTimer();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentQuestion < quizzes.size()) {
                    setQuestion(quizzes.get(currentQuestion), difficult);
                } else {
                    training1Translate.endTraning(getFragmentManager(), String.valueOf(Integer.parseInt(CorrectCount.getText().toString())), String.valueOf(Integer.parseInt(IncorrectCount.getText().toString())));
                }
                nextButton.setVisibility(View.GONE);
                submButton.setVisibility(View.VISIBLE);
                if (difficult == 2) {
                    training1Translate.startTimer(timeLeftCount, submButton, quizzes.get(currentQuestion).getCorrectCount() + 45);
                }
            }
        });
        rootView.findViewById(R.id.end_train).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                training1Translate.endTraning(getFragmentManager(),String.valueOf(Integer.parseInt(CorrectCount.getText().toString())),String.valueOf(Integer.parseInt(IncorrectCount.getText().toString())));
            }
        });

        return rootView;
    }

    private void checkAnswer() {
        StringBuilder sb = new StringBuilder();
        int cor = 0;
        for (int i = 0; i < mRecyclerView.getChildCount(); i++) {
            String currLetter = String.valueOf(((TextView)
                    ((RelativeLayout)
                            ((CardView)
                                    ((RelativeLayout) mRecyclerView.getChildAt(i))
                                            .getChildAt(0)).getChildAt(0)).getChildAt(0)).getText());
            if (currLetter.equals(correctArrayList.get(i))) {
                cor++;
                (((CardView)
                        ((RelativeLayout) mRecyclerView.getChildAt(i))
                                .getChildAt(0)).getChildAt(0)).setBackgroundColor(Color.parseColor("#00c853"));
            } else {
                (((CardView)
                        ((RelativeLayout) mRecyclerView.getChildAt(i))
                                .getChildAt(0)).getChildAt(0)).setBackgroundColor(Color.parseColor("#b71c1c"));
            }
        }
        if (cor == correctArrayList.size()) {
            CorrectCount.setText(String.valueOf(Integer.parseInt(CorrectCount.getText().toString()) + 1));
        } else IncorrectCount.setText(String.valueOf(Integer.parseInt(IncorrectCount.getText().toString()) + 1));
        for (String aCorrectArrayList : correctArrayList) {
            sb.append(aCorrectArrayList);
        }
        variantTV.setText(sb.toString());
    }

    private void setQuestion(TrainingTrans.Quiz quiz, int difficult) {
        charQuestionList.clear();
        correctArrayList.clear();
        Random r = new Random();
        curQuestString = quiz.getCorrectAnswer().get(r.nextInt(quiz.getCorrectCount()));
        char question[] = curQuestString.toCharArray();
        for (int i = 0; i < curQuestString.length(); i++) {
            charQuestionList.add(String.valueOf(question[i]));
            correctArrayList.add(String.valueOf(question[i]));
        }
        Collections.shuffle(charQuestionList);
        if (difficult == 0 || difficult == 1) variantTV.setText(quiz.getQuestion());
        initRecyclerView(mRecyclerView);
    }

    private void initRecyclerView(RecyclerView recyclerView) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        final RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), charQuestionList, "Training3Shuffle");
        recyclerView.setAdapter(adapter);

        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, 0) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder dragged, @NonNull RecyclerView.ViewHolder target) {
                int position_dragged = dragged.getAdapterPosition();
                int position_targent = target.getAdapterPosition();

                Collections.swap(charQuestionList, position_dragged, position_targent);

                adapter.notifyItemMoved(position_dragged, position_targent);

                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

            }
        });
        helper.attachToRecyclerView(recyclerView);
    }
}
