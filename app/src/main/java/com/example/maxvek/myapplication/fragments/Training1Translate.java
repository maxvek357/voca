package com.example.maxvek.myapplication.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.maxvek.myapplication.R;
import com.example.maxvek.myapplication.utils.TrainingTrans;
import com.example.maxvek.myapplication.repository.DatabaseInstance;

import java.util.*;

public class Training1Translate extends Fragment {
    private String fromSpinner, toSpinner;
    private TextView textView, CorrectCount, IncorrectCount, timeLeftCount;
    private RadioGroup radioGroup;
    private int difficult, CurrentQuestion = 0;
    private LinearLayout linearLayout;
    private Button nextBut, submBut;
    private CountDownTimer countDownTimer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.training_1_translate, container, false);

        final Bundle bundle = this.getArguments();
        if (bundle != null) {
            fromSpinner = bundle.getString("fromSpinner");
            toSpinner = bundle.getString("toSpinner");
            difficult = bundle.getInt("difficult");
        }
        textView = rootView.findViewById(R.id.question);
        CorrectCount = rootView.findViewById(R.id.correct_count_tview);
        IncorrectCount = rootView.findViewById(R.id.incorrect_count_tview);
        timeLeftCount = rootView.findViewById(R.id.time_left_count);
        radioGroup = rootView.findViewById(R.id.radio_group_translate);
        linearLayout = rootView.findViewById(R.id.linear_for_checkbox_li);
        nextBut = rootView.findViewById(R.id.next_train_transl);
        submBut = rootView.findViewById(R.id.subm_train_transl);

        setDiffText((TextView) rootView.findViewById(R.id.difficult_type));

        final List<TrainingTrans.Quiz> quizzes = TrainingTrans.getWords(fromSpinner, toSpinner, difficult);
        Collections.shuffle(quizzes);

        if (difficult == 0 || difficult == 1) {
            setRadioButton(quizzes.get(CurrentQuestion).getIncorrectCount());
            setView(quizzes.get(CurrentQuestion), radioGroup);
        } else {
            linearLayout.setVisibility(View.VISIBLE);
            (rootView.findViewById(R.id.time_left_text)).setVisibility(View.VISIBLE);
            timeLeftCount.setVisibility(View.VISIBLE);
            setCheckBox(quizzes.get(CurrentQuestion).getIncorrectCount() + quizzes.get(CurrentQuestion).getCorrectCount());
            setView(quizzes.get(CurrentQuestion), linearLayout);
            startTimer(timeLeftCount, submBut, quizzes.get(CurrentQuestion).getIncorrectCount() + quizzes.get(CurrentQuestion).getCorrectCount() + 15);
        }

        submBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (difficult == 0 || difficult == 1) {
                    checkAnswer(quizzes.get(CurrentQuestion));
                } else {
                    stopTimer();
                    checkAnswerBox(quizzes.get(CurrentQuestion));
                }
                CurrentQuestion++;
                nextBut.setVisibility(View.VISIBLE);
                submBut.setVisibility(View.GONE);
            }
        });
        nextBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CurrentQuestion < quizzes.size()) {
                    if (difficult == 0 || difficult == 1) {
                        radioGroup.clearCheck();
                        clearColorView(radioGroup);
                        setView(quizzes.get(CurrentQuestion), radioGroup);
                    } else {
                        setCheckBox(quizzes.get(CurrentQuestion).getIncorrectCount() + quizzes.get(CurrentQuestion).getCorrectCount());
                        clearColorView(linearLayout);
                        setView(quizzes.get(CurrentQuestion), linearLayout);
                        startTimer(timeLeftCount, submBut, quizzes.get(CurrentQuestion).getIncorrectCount() + quizzes.get(CurrentQuestion).getCorrectCount() + 15);
                    }
                    nextBut.setVisibility(View.GONE);
                    submBut.setVisibility(View.VISIBLE);
                } else {
                    endTraning(getFragmentManager(), String.valueOf(Integer.parseInt(CorrectCount.getText().toString())), String.valueOf(Integer.parseInt(IncorrectCount.getText().toString())));
                }
            }
        });
        rootView.findViewById(R.id.end_train).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                endTraning(getFragmentManager(), String.valueOf(Integer.parseInt(CorrectCount.getText().toString())), String.valueOf(Integer.parseInt(IncorrectCount.getText().toString())));
            }
        });
        return rootView;
    }

    public void endTraning(FragmentManager fragmentManager, String corCount, String incorCount) {
        Fragment fragment = new ResultFragment();
        fragmentManager.beginTransaction().replace(R.id.include_indrawer, fragment).addToBackStack(null).commit();
        Bundle bundle = new Bundle();
        bundle.putString("CorrectCount", corCount);
        bundle.putString("IncorrectCount", incorCount);
        fragment.setArguments(bundle);
    }

    public void setDiffText(TextView textDiff) {
        int difficultStr = R.string.easy_diff;
        switch (difficult) {
            case 0:
                difficultStr = R.string.easy_diff;
                break;
            case 1:
                difficultStr = R.string.norm_diff;
                break;
            case 2:
                difficultStr = R.string.hard_diff;
                break;
        }
        textDiff.setText(difficultStr);
    }

    public void startTimer(final TextView textView, final TextView endView, int sec) {
        textView.setText(String.valueOf(sec));

        countDownTimer = new CountDownTimer(sec * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                textView.setText(String.valueOf(millisUntilFinished / 1000));
            }

            public void onFinish() {
                endView.performClick();
            }
        };

        countDownTimer.start();
    }

    public void stopTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    private void statOfWord(String word, String lang, boolean answer) {
        DatabaseInstance.getInstance().getMyDB().insertStatWord(word, lang, answer);
    }

    private void clearColorView(ViewGroup view) {
        if (view instanceof RadioGroup) {
            for (int i = 0; i < view.getChildCount(); i++) {
                ((RadioButton) view.getChildAt(i)).setTextColor(Color.BLACK);
            }
        } else {
            for (int i = 0; i < view.getChildCount(); i++) {
                ((CheckBox) view.getChildAt(i)).setTextColor(Color.BLACK);
            }
        }

    }

    private void setView(TrainingTrans.Quiz quizzes, View view) {
        textView.setText(quizzes.getQuestion());
        Set<String> setList = new HashSet<>();
        setList.addAll(quizzes.getCorrectAnswer());
        setList.addAll(quizzes.getIncorrectAnswer());
        List<String> list = new ArrayList<>(setList);
        Collections.shuffle(list);
        if (view instanceof RadioGroup) {
            for (int i = 0; i < list.size(); i++) {
                ((RadioButton) radioGroup.getChildAt(i)).setText(list.get(i));
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                ((CheckBox) linearLayout.getChildAt(i)).setText(list.get(i));
            }
        }
    }

    private void checkAnswer(TrainingTrans.Quiz quizzes) {
        RadioButton r = (RadioButton) radioGroup.getChildAt(radioGroup.indexOfChild(getActivity().findViewById(radioGroup.getCheckedRadioButtonId())));
        if (r != null) {
            String answer = r.getText().toString();
            if (answer.equals(quizzes.getCorrectAnswer().get(0))) {
                CorrectCount.setText(String.valueOf(Integer.parseInt(CorrectCount.getText().toString()) + 1));
                r.setTextColor(Color.parseColor("#00c853"));
                statOfWord(quizzes.getQuestion(), fromSpinner, true);
            } else {
                IncorrectCount.setText(String.valueOf(Integer.parseInt(IncorrectCount.getText().toString()) + 1));
                r.setTextColor(Color.parseColor("#b71c1c"));
                statOfWord(quizzes.getQuestion(), fromSpinner, false);
                for (int i = 0; i < radioGroup.getChildCount(); i++) {
                    RadioButton child = (RadioButton) (radioGroup.getChildAt(i));
                    if (child.getText().toString().equals(quizzes.getCorrectAnswer().get(0))) {
                        child.setTextColor(Color.parseColor("#00c853"));
                    }
                }
            }
        } else {
            IncorrectCount.setText(String.valueOf(Integer.parseInt(IncorrectCount.getText().toString()) + 1));
            statOfWord(quizzes.getQuestion(), fromSpinner, false);
            for (int i = 0; i < radioGroup.getChildCount(); i++) {
                RadioButton child = (RadioButton) (radioGroup.getChildAt(i));
                if (child.getText().toString().equals(quizzes.getCorrectAnswer().get(0))) {
                    child.setTextColor(Color.parseColor("#00c853"));
                }
            }
        }

    }

    private void checkAnswerBox(TrainingTrans.Quiz quizzes) {
        int correctAnswer = quizzes.getCorrectCount();
        int correctCount = 0;
        for (int i = 0; i < linearLayout.getChildCount(); i++) {
            String answer = ((CheckBox) linearLayout.getChildAt(i)).getText().toString();
            if (((CheckBox) linearLayout.getChildAt(i)).isChecked()) {
                boolean wasCorrect = false;
                for (String s : quizzes.getCorrectAnswer()) {
                    if (s.equals(answer)) wasCorrect = true;
                }
                if (wasCorrect) {
                    correctCount++;
                    ((CheckBox) linearLayout.getChildAt(i)).setTextColor(Color.parseColor("#00c853"));
                } else {
                    ((CheckBox) linearLayout.getChildAt(i)).setTextColor(Color.parseColor("#b71c1c"));
                }
            } else {
                for (String s : quizzes.getCorrectAnswer()) {
                    if (s.equals(answer)) {
                        ((CheckBox) linearLayout.getChildAt(i)).setTextColor(Color.parseColor("#00c853"));
                    }
                }
            }
        }
        if (correctAnswer == correctCount) {
            CorrectCount.setText(String.valueOf(Integer.parseInt(CorrectCount.getText().toString()) + 1));
        } else {
            IncorrectCount.setText(String.valueOf(Integer.parseInt(IncorrectCount.getText().toString()) + 1));
        }
    }

    private void setRadioButton(int buttons) {
        radioGroup.setVisibility(View.VISIBLE);
        for (int i = 0; i <= buttons; i++) {
            RadioButton rbn = new RadioButton(getContext());
            radioGroup.addView(rbn);
        }
    }

    private void setCheckBox(int buttons) {
        linearLayout.removeAllViews();
        for (int i = 0; i < buttons; i++) {
            CheckBox checkBox = new CheckBox(getContext());
            checkBox.setId(i + 6);
            linearLayout.addView(checkBox);
        }
    }
}
