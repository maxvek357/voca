package com.example.maxvek.myapplication.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.maxvek.myapplication.R;
import com.example.maxvek.myapplication.models.StatInt;
import com.example.maxvek.myapplication.repository.DatabaseInstance;
import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.charts.StackedBarChart;
import org.eazegraph.lib.models.BarModel;
import org.eazegraph.lib.models.PieModel;
import org.eazegraph.lib.models.StackedBarModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;
import static com.example.maxvek.myapplication.fragments.ContextMain.initSpinnerFrom;
import static com.example.maxvek.myapplication.utils.DatabaseHelper.TABLE_STAT_ALL;
import static com.example.maxvek.myapplication.utils.DatabaseHelper.TABLE_STAT_WORD;

public class StatisticFragment extends Fragment {
    private StackedBarChart mStackedBarChart, mStackedBarChart2;
    private PieChart mPieChart, mPieChart2;
    private LinearLayout linearLayout;
    private int page = 2;
    private AutoCompleteTextView editText;
    private TextView titleTV;

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @android.support.annotation.Nullable ViewGroup container, @android.support.annotation.Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.statistic_layout, container, false);
        mStackedBarChart = rootView.findViewById(R.id.stackedbarchart);
        mStackedBarChart2 = rootView.findViewById(R.id.stackedbarchart2);
        mPieChart = rootView.findViewById(R.id.piechart);
        mPieChart2 = rootView.findViewById(R.id.piechart2);
        titleTV = rootView.findViewById(R.id.stat_tw);
        final Spinner fromSpinner = rootView.findViewById(R.id.from_lang_spinner);
        linearLayout = rootView.findViewById(R.id.stat_linear_layout);
        editText = rootView.findViewById(R.id.stat_edit_text);

        titleTV.setText(R.string.stat_title_word);

        initSpinnerFrom(rootView);
        rootView.findViewById(R.id.to_lang_text_view).setVisibility(View.GONE);
        rootView.findViewById(R.id.to_lang_spinner).setVisibility(View.GONE);

        final List<StatInt> statInts = DatabaseInstance.getInstance().getMyDB().getChartInfo(TABLE_STAT_ALL, "");
        if (!statInts.isEmpty()) {
            setBarChart(statInts, false);
            setPieChart(statInts);
        }

        rootView.findViewById(R.id.stat_but_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                page--;
                if (page < 0) page = 2;
                setStatisticPage();
            }
        });

        rootView.findViewById(R.id.stat_but_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                page++;
                if (page > 2) page = 0;
                setStatisticPage();
            }
        });

        rootView.findViewById(R.id.stat_word_but).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mStackedBarChart2.setVisibility(View.GONE);
                mPieChart2.setVisibility(View.VISIBLE);
                StatInt statInt = DatabaseInstance.getInstance().getMyDB().getWordStat(editText.getText().toString().trim(), fromSpinner.getSelectedItem().toString());
                if (statInt != null) {
                    setPieChartWord(statInt);
                    mPieChart2.startAnimation();
                }
            }
        });

        rootView.findViewById(R.id.image_view_stt_stat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, Locale.getDefault());

                if (intent.resolveActivity(Objects.requireNonNull(getActivity()).getPackageManager()) != null) {
                    startActivityForResult(intent, 10);
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.speech_to_text), Toast.LENGTH_SHORT).show();
                }
            }
        });

        final VocabularyFragment vocabularyFragment = new VocabularyFragment();

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                vocabularyFragment.showHintText(editText, fromSpinner, Objects.requireNonNull(getContext()));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        fromSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                showWordStat(fromSpinner);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                showWordStat(fromSpinner);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    editText.setText(result.get(0));
                }
                break;
        }
    }

    private void showWordStat(Spinner fromSpinner){
        mStackedBarChart2.setVisibility(View.VISIBLE);
        mPieChart2.setVisibility(View.GONE);
        mStackedBarChart2.clearChart();
        final List<StatInt> statIntsWord = DatabaseInstance.getInstance().getMyDB().getChartInfo(TABLE_STAT_WORD, fromSpinner.getSelectedItem().toString());
        if (!statIntsWord.isEmpty()) setBarChart(statIntsWord, true);
        mStackedBarChart2.startAnimation();
    }

    private void setStatisticPage() {
        switch (page) {
            case 0:
                mStackedBarChart.setVisibility(View.VISIBLE);
                mPieChart.setVisibility(View.GONE);
                linearLayout.setVisibility(View.GONE);
                mStackedBarChart.startAnimation();
                titleTV.setText(R.string.stat_title_last5);
                break;
            case 1:
                mPieChart.setVisibility(View.VISIBLE);
                mStackedBarChart.setVisibility(View.GONE);
                linearLayout.setVisibility(View.GONE);
                mPieChart.startAnimation();
                titleTV.setText(R.string.stat_title_all);
                break;
            case 2:
                linearLayout.setVisibility(View.VISIBLE);
                mStackedBarChart.setVisibility(View.GONE);
                mPieChart.setVisibility(View.GONE);
                mStackedBarChart2.startAnimation();
                titleTV.setText(R.string.stat_title_word);

                break;
        }
    }

    private void setBarChart(List<StatInt> statInts, Boolean type) {
        if (type) {
            mPieChart2.clearChart();
            int k = statInts.size();
            if (k > 5) k = statInts.size() - (statInts.size() - 5);
            for (int i = 0; i < k; i++) {
                StackedBarModel s = new StackedBarModel(statInts.get(i).getWord());
                s.addBar(new BarModel(statInts.get(i).getCor(), Color.parseColor("#00c853")));
                s.addBar(new BarModel(statInts.get(i).getIncor(), Color.parseColor("#b71c1c")));
                mStackedBarChart2.addBar(s);
            }
        } else {
            for (int i = statInts.size(); i > (statInts.size() - 5); i--) {
                StackedBarModel s = new StackedBarModel("#" + String.valueOf(i));
                s.addBar(new BarModel(statInts.get(i - 1).getCor(), Color.parseColor("#00c853")));
                s.addBar(new BarModel(statInts.get(i - 1).getIncor(), Color.parseColor("#b71c1c")));
                mStackedBarChart.addBar(s);
            }
        }
    }

    private void setPieChartWord(StatInt statInt) {
        if (statInt != null) {
            mPieChart2.clearChart();
            mPieChart2.addPieSlice(new PieModel(getResources().getString(R.string.stat_cor) + " " + statInt.word, statInt.getCor(), Color.parseColor("#00c853")));
            mPieChart2.addPieSlice(new PieModel(getResources().getString(R.string.stat_incor) + " " + statInt.word, statInt.getIncor(), Color.parseColor("#b71c1c")));
        }
    }

    private void setPieChart(List<StatInt> statInts) {
        int cor = 0;
        int incor = 0;
        for (StatInt statInt : statInts) {
            cor += statInt.getCor();
            incor += statInt.getIncor();
        }
        mPieChart.addPieSlice(new PieModel(getResources().getString(R.string.stat_cor), cor, Color.parseColor("#00c853")));
        mPieChart.addPieSlice(new PieModel(getResources().getString(R.string.stat_incor), incor, Color.parseColor("#b71c1c")));
    }

}
