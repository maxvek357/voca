package com.example.maxvek.myapplication.models;

public class Phrase {
    public String text;

    String language;

    @Override
    public String toString() {
        return "ClassPojo [text = " + text + ", language = " + language + "]";
    }
}