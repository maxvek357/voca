package com.example.maxvek.myapplication.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.maxvek.myapplication.R;
import com.example.maxvek.myapplication.repository.DatabaseInstance;

import javax.annotation.Nullable;

public class ResultFragment extends Fragment {
    private String correctCount, incorrectCount;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @android.support.annotation.Nullable ViewGroup container, @android.support.annotation.Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.result_fragment, container, false);

        final Bundle bundle = this.getArguments();
        if (bundle != null) {
            correctCount = bundle.getString("CorrectCount");
            incorrectCount = bundle.getString("IncorrectCount");
        }
        TextView correctView = rootView.findViewById(R.id.res_correct_count_tview);
        TextView incorrectView = rootView.findViewById(R.id.res_incorrect_count_tview);

        int corInt = Integer.parseInt(correctCount);
        int incorInt = Integer.parseInt(incorrectCount);

        if ((corInt != 0)||(incorInt != 0)){
            DatabaseInstance.getInstance().getMyDB().insertStatAll(corInt, incorInt);
        }

        correctView.setText(correctCount);
        incorrectView.setText(incorrectCount);

        rootView.findViewById(R.id.result_back_but).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new SelectTraining();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.include_indrawer, fragment).addToBackStack(null).commit();
            }
        });
        return rootView;
    }
}
