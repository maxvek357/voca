package com.example.maxvek.myapplication.models;

public class TranslateModel {

    public String result;

    public String phrase;

    public Tuc[] tuc;

    @Override
    public String toString() {
        return "ClassPojo [result = " + result + ", phrase = " + phrase + ", tuc = " + tuc + "]";
    }
}