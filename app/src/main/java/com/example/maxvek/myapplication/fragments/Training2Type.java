package com.example.maxvek.myapplication.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.example.maxvek.myapplication.R;
import com.example.maxvek.myapplication.utils.TrainingTrans;

import java.util.*;

public class Training2Type extends Fragment {
    private String fromSpinner, toSpinner, curQuestString;
    private int difficult, currentQuestion = 0;
    private TextView textQuest, textAnsw, CorrectCount, IncorrectCount, checkAnswerTV, timeLeftCount;
    private Button submBut, nextBut;
    private EditText editText;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.training_2_type, container, false);

        final Bundle bundle = this.getArguments();
        if (bundle != null) {
            fromSpinner = bundle.getString("fromSpinner");
            toSpinner = bundle.getString("toSpinner");
            difficult = bundle.getInt("difficult");
        }
        CorrectCount = rootView.findViewById(R.id.correct_count_tview);
        IncorrectCount = rootView.findViewById(R.id.incorrect_count_tview);
        textQuest = rootView.findViewById(R.id.question_tr_type);
        textAnsw = rootView.findViewById(R.id.answer_tr_type);
        submBut = rootView.findViewById(R.id.subm_train_type);
        nextBut = rootView.findViewById(R.id.next_train_type);
        checkAnswerTV = rootView.findViewById(R.id.check_answer_type);
        timeLeftCount = rootView.findViewById(R.id.time_left_count);
        editText = rootView.findViewById(R.id.edit_tr_type);

        final Training1Translate training1Translate = new Training1Translate();

        training1Translate.setDiffText((TextView) rootView.findViewById(R.id.difficult_type));


        final List<TrainingTrans.Quiz> quizzes = TrainingTrans.getWords(fromSpinner, toSpinner);
        Collections.shuffle(quizzes);

        setQuestion(quizzes.get(currentQuestion), difficult);
        if (difficult == 2) {
            textQuest.setVisibility(View.GONE);
            (rootView.findViewById(R.id.time_left_text)).setVisibility(View.VISIBLE);
            timeLeftCount.setVisibility(View.VISIBLE);
            training1Translate.startTimer(timeLeftCount, submBut, quizzes.get(currentQuestion).getCorrectCount() + 15);
        }

        submBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextBut.setVisibility(View.VISIBLE);
                checkAnswerTV.setVisibility(View.VISIBLE);
                submBut.setVisibility(View.GONE);
                editText.setVisibility(View.GONE);
                if (difficult == 2) {
                    training1Translate.stopTimer();
                    textQuest.setVisibility(View.VISIBLE);
                }
                checkAnswer(quizzes.get(currentQuestion));
                currentQuestion++;
            }
        });
        nextBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentQuestion < quizzes.size()) {
                    setQuestion(quizzes.get(currentQuestion), difficult);
                    if (difficult == 2) {
                        textQuest.setVisibility(View.GONE);
                        training1Translate.startTimer(timeLeftCount, submBut, quizzes.get(currentQuestion).getCorrectCount() + 15);
                    }
                    nextBut.setVisibility(View.GONE);
                    checkAnswerTV.setVisibility(View.GONE);
                    submBut.setVisibility(View.VISIBLE);
                    editText.setVisibility(View.VISIBLE);
                    editText.setText("");
                } else {
                    training1Translate.endTraning(getFragmentManager(), String.valueOf(Integer.parseInt(CorrectCount.getText().toString())), String.valueOf(Integer.parseInt(IncorrectCount.getText().toString())));
                }
            }
        });
        rootView.findViewById(R.id.end_train).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                training1Translate.endTraning(getFragmentManager(), String.valueOf(Integer.parseInt(CorrectCount.getText().toString())), String.valueOf(Integer.parseInt(IncorrectCount.getText().toString())));
            }
        });
        return rootView;
    }


    private void checkAnswer(TrainingTrans.Quiz quiz) {
        String answerET = editText.getText().toString().trim();
        if (difficult == 0 || difficult == 1) {
            SpannableString answer = new SpannableString(answerET);
            if (answer.toString().equals(curQuestString)) {
                CorrectCount.setText(String.valueOf(Integer.parseInt(CorrectCount.getText().toString()) + 1));
                checkAnswerTV.setTextColor(Color.parseColor("#00c853"));
                checkAnswerTV.setText(answer);
            } else {
                char answerChar[] = answer.toString().toCharArray();
                char corAnswerChar[] = curQuestString.toCharArray();
                if (answer.length() > curQuestString.length()) {
                    for (int i = 0; i < curQuestString.length(); i++) {
                        if (answerChar[i] == corAnswerChar[i]) {
                            answer.setSpan(new ForegroundColorSpan(Color.parseColor("#00c853")), i, i + 1, 0);
                        } else {
                            answer.setSpan(new ForegroundColorSpan(Color.parseColor("#b71c1c")), i, i + 1, 0);
                        }
                    }
                    answer.setSpan(new ForegroundColorSpan(Color.parseColor("#b71c1c")), curQuestString.length(), answer.length(), 0);
                } else {
                    for (int i = 0; i < answer.length(); i++) {
                        if (answerChar[i] == corAnswerChar[i]) {
                            answer.setSpan(new ForegroundColorSpan(Color.parseColor("#00c853")), i, i + 1, 0);
                        } else {
                            answer.setSpan(new ForegroundColorSpan(Color.parseColor("#b71c1c")), i, i + 1, 0);
                        }
                    }
                }
                checkAnswerTV.setText(answer);
                textQuest.setText(curQuestString);
                IncorrectCount.setText(String.valueOf(Integer.parseInt(IncorrectCount.getText().toString()) + 1));
            }
        } else {
            StringBuffer sb = new StringBuffer();
            boolean wasCorrect = false;
            for (String s : quiz.getCorrectAnswer()) {
                if (answerET.equals(s)) {
                    CorrectCount.setText(String.valueOf(Integer.parseInt(CorrectCount.getText().toString()) + 1));
                    checkAnswerTV.setTextColor(Color.parseColor("#00c853"));
                    checkAnswerTV.setText(answerET);
                    wasCorrect = true;
                }
                sb.append(s).append(" - ");
            }
            if (!wasCorrect) {
                IncorrectCount.setText(String.valueOf(Integer.parseInt(IncorrectCount.getText().toString()) + 1));
                checkAnswerTV.setTextColor(Color.parseColor("#b71c1c"));
                checkAnswerTV.setText(answerET);
            }
            textQuest.setText(sb.toString());
        }
    }

    private void setQuestion(TrainingTrans.Quiz quiz, int difficult) {
        if (difficult == 0 || difficult == 1) {
            Random r = new Random();
            curQuestString = quiz.getCorrectAnswer().get(r.nextInt(quiz.getCorrectCount()));
            int perc = 40;
            if (difficult == 1) {
                perc = 60;
            }
            int k = (int) (curQuestString.length() * (perc / 100.0f));
            Set<Integer> num = new HashSet<>();
            StringBuilder question = new StringBuilder(curQuestString);
            for (int i = 0; i < k; i++) {
                question.setCharAt(checkNum(curQuestString.length(), num), '_');
            }
            textQuest.setText(question.toString());
        }
        textAnsw.setText(quiz.getQuestion());
    }

    private int checkNum(int length, Set<Integer> num) {
        Random r = new Random();
        int cRand = r.nextInt(length);
        while (num.contains(cRand)) {
            cRand = r.nextInt(length);
        }
        num.add(cRand);
        return cRand;
    }
}
