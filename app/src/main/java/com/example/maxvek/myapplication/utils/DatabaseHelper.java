package com.example.maxvek.myapplication.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.maxvek.myapplication.models.StatInt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String TABLE_STAT_WORD = "stat_word";
    public static final String TABLE_STAT_ALL = "stat_all";
    private static final String DATABASE_NAME = "vocabulary.db";
    private static final String TABLE_NAME = "translate";
    private static final String COL_ID = "id";
    private static final String COL_UK = "uk";
    private static final String COL_EN = "en";
    private static final String COL_RU = "ru";
    private static final String COL_DE = "de";
    private static final String COL_FR = "fr";
    private SQLiteDatabase db = this.getWritableDatabase();


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + TABLE_NAME + " (id INTEGER PRIMARY KEY AUTOINCREMENT,uk TEXT, en TEXT, ru TEXT, de TEXT, fr TEXT)");
        sqLiteDatabase.execSQL("create table " + TABLE_STAT_WORD + " (id INTEGER PRIMARY KEY AUTOINCREMENT,uk TEXT, en TEXT, ru TEXT, de TEXT, fr TEXT, cor INTEGER, incor INTEGER)");
        sqLiteDatabase.execSQL("create table " + TABLE_STAT_ALL + " (id INTEGER PRIMARY KEY AUTOINCREMENT,cor INTEGER, incor INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STAT_ALL);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
        onCreate(db);
    }

    public boolean insertData(String from, String to, String word, String translate) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(from, word);
        contentValues.put(to, translate);
        long result = db.insert(TABLE_NAME, null, contentValues);
        return result != -1;
    }

    public boolean insertStatAll(int cor, int incor) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("cor", cor);
        contentValues.put("incor", incor);
        long result = db.insert(TABLE_STAT_ALL, null, contentValues);
        return result != -1;
    }

    public void insertStatWord(String word, String lang, Boolean answer) {
        Cursor res = db.rawQuery("select id from " + TABLE_NAME + " where " + lang + " = '" + word + "' limit(1)", null);
        String id = "";
        if (res.moveToFirst()) {
            id = res.getString(res.getColumnIndex("id"));
        }
        res.close();
        if (id.isEmpty())
            return;
        if (answer) {
            db.execSQL("update " + TABLE_STAT_WORD + " set cor = cor + 1 WHERE " + lang + " = " + id);
            long update = DatabaseUtils.longForQuery(db, "SELECT changes()", null);
            if (update == 0) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(lang, id);
                contentValues.put("cor", 1);
                contentValues.put("incor", 0);
                db.insert(TABLE_STAT_WORD, null, contentValues);
            }
        } else {
            db.execSQL("update " + TABLE_STAT_WORD + " set incor = incor + 1 WHERE " + lang + " = " + id);
            long update = DatabaseUtils.longForQuery(db, "SELECT changes()", null);
            if (update == 0) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(lang, id);
                contentValues.put("incor", 1);
                contentValues.put("cor", 0);
                db.insert(TABLE_STAT_WORD, null, contentValues);
            }
        }
    }

    public List<StatInt> getChartInfo(String tableName, String lang) {
        List<StatInt> list = new ArrayList<>();
        if (tableName.equals(TABLE_STAT_ALL)) {
            Cursor res = db.rawQuery("select cor, incor from " + tableName, null);
            while (res.moveToNext()) {
                StatInt statInt = new StatInt();
                statInt.cor = res.getInt(0);
                statInt.incor = res.getInt(1);
                statInt.word = "";
                list.add(statInt);
            }
        } else {
            Cursor res = db.rawQuery("select " + lang + ", cor, incor from " + tableName + " where " + lang + " is not null ORDER BY RANDOM()LIMIT 5", null);
            while (res.moveToNext()) {
                StatInt statInt = new StatInt();
                statInt.cor = res.getInt(res.getColumnIndex("cor"));
                statInt.incor = res.getInt(res.getColumnIndex("incor"));
                Cursor sel = db.rawQuery("select " + lang + " from " + TABLE_NAME + " where id = " + res.getString(res.getColumnIndex(lang)),null);
                if (sel.moveToFirst()) {
                    statInt.word = sel.getString(0);
                }
                sel.close();
                list.add(statInt);
            }
        }
        return list;
    }

    public Map<String, List<String>> getAllForVocabulary(String from, String to) {
        Cursor res = db.rawQuery("select " + from + ", " + to + " from " + TABLE_NAME + " where " + from + " is not null and " + to + " is not null", null);
        HashMap<String, List<String>> hmapVoc = new HashMap<>();
        while (res.moveToNext()) {
            if (hmapVoc.containsKey(res.getString(0))) {
                hmapVoc.get(res.getString(0)).add(res.getString(1));
            } else {
                ArrayList<String> values = new ArrayList<>();
                values.add(res.getString(1));
                hmapVoc.put(res.getString(0), values);
            }
        }
        res.close();
        return hmapVoc;
    }

    public Cursor getWordData(String lang, String word) {
        return db.rawQuery("select * from " + TABLE_NAME + " where " + lang + " = '" + word + "'", null);
    }

    public Cursor getAllForFile(){
        return db.rawQuery("select uk,en,ru,de,fr from " + TABLE_NAME, null);
    }

    public StatInt getWordStat(String word, String lang) {
        Cursor sel = db.rawQuery("select id from " + TABLE_NAME + " where " + lang + " = '" + word + "'", null);
        String id = "";
        if (sel.moveToFirst()) {
            id = sel.getString(sel.getColumnIndex("id"));
        }
        sel.close();
        StatInt statInt = new StatInt();
        if (!id.isEmpty()) {
            Cursor res = db.rawQuery("select * from " + TABLE_STAT_WORD + " where " + lang + " = '" + id + "'", null);
            if (res.moveToFirst()) {
                statInt.cor = res.getInt(res.getColumnIndex("cor"));
                statInt.incor = res.getInt(res.getColumnIndex("incor"));
                statInt.word = word;
            }
            res.close();
        }
        return statInt;
    }

    public ArrayList<String> getAllColum() {
        Cursor res = db.rawQuery("PRAGMA table_info(" + TABLE_NAME + ")", null);
        ArrayList<String> list = new ArrayList<>();
        while (res.moveToNext()) {
            list.add(res.getString(res.getColumnIndex("name")));
        }
        list.remove(0);
        res.close();
        return list;
    }

    public List<String> getHint(String word, String lang){
        Cursor res = db.rawQuery("select distinct " + lang + " from " + TABLE_NAME + " where " + lang + " like '%" + word + "%'",null);
        List<String> list = new ArrayList<>();
        if (res.moveToFirst()){
            while (res.moveToNext()){
                list.add(res.getString(0));
            }
        }
        return list;
    }

    public boolean updateData(String id, String uk, String en, String ru, String de) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_UK, uk);
        contentValues.put(COL_EN, en);
        contentValues.put(COL_RU, ru);
        contentValues.put(COL_DE, de);
        db.update(TABLE_NAME, contentValues, "ID = ?", new String[]{id});
        return true;
    }

    public void deleteData(String from, String word) {
        Cursor res = db.rawQuery("select id from " + TABLE_NAME + " where " + from + " = '" + word + "'", null);
        if (res.moveToFirst()){
            String id = res.getString(0);
            if (!id.isEmpty()){
                db.delete(TABLE_STAT_WORD, from + " = ?", new  String[]{id});
                db.delete(TABLE_NAME, from + " = ?", new String[]{word});
            }
        }
    }

    public void deleteAll(String table){
        db.execSQL("delete from " + table);
    }

    public boolean importDatabase(String dbPath) throws IOException {
        close();
        File newDb = new File(dbPath);
        File oldDb = new File("/data/data/com.example.maxvek.myapplication/databases/vocabulary.db");
        if (newDb.exists()) {
            FileUtils.copyFile(new FileInputStream(newDb), new FileOutputStream(oldDb));
            db = SQLiteDatabase.openDatabase("/data/data/com.example.maxvek.myapplication/databases/vocabulary.db", null,
                    SQLiteDatabase.OPEN_READWRITE);
            deleteAll(TABLE_STAT_WORD);
            deleteAll(TABLE_STAT_ALL);
            return true;
        }
        return false;
    }

    public boolean undoImportDatabase() throws IOException{
        close();
        File newDb = new File("/data/data/com.example.maxvek.myapplication/files/vocabulary.db");
        File oldDb = new File("/data/data/com.example.maxvek.myapplication/databases/vocabulary.db");
        if (newDb.exists()) {
            FileUtils.copyFile(new FileInputStream(newDb), new FileOutputStream(oldDb));
            db = SQLiteDatabase.openDatabase("/data/data/com.example.maxvek.myapplication/databases/vocabulary.db", null,
                    SQLiteDatabase.OPEN_READWRITE);
            return true;
        }
        return false;
    }
}
