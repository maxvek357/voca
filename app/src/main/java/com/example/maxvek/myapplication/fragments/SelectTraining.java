package com.example.maxvek.myapplication.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.example.maxvek.myapplication.R;
import com.example.maxvek.myapplication.activity.MainActivity;
import com.example.maxvek.myapplication.repository.DatabaseInstance;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.example.maxvek.myapplication.fragments.ContextMain.initSpinnerFrom;
import static com.example.maxvek.myapplication.fragments.ContextMain.initSpinnerTo;

public class SelectTraining extends Fragment {
    private Spinner fromSpinner, toSpinner;
    private RadioGroup radioGroup;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_select_training, container, false);
        fromSpinner = rootView.findViewById(R.id.from_lang_spinner);
        toSpinner = rootView.findViewById(R.id.to_lang_spinner);
        radioGroup = rootView.findViewById(R.id.select_diff_rg);
        initSpinnerTo(rootView);
        initSpinnerFrom(rootView);
        rootView.findViewById(R.id.training_trans_but).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentTrigger(new Training1Translate());
            }
        });
        rootView.findViewById(R.id.training_blank_but).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentTrigger(new Training2Type());
            }
        });
        rootView.findViewById(R.id.training_choice_but).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentTrigger(new Training3Shuffle());
            }
        });
        return rootView;
    }

    private void fragmentTrigger(Fragment fragment) {
        Map<String, List<String>> hmapVoc = DatabaseInstance.getInstance().getMyDB().getAllForVocabulary(fromSpinner.getSelectedItem().toString(), toSpinner.getSelectedItem().toString());
        if (!hmapVoc.isEmpty()) {
            FragmentManager fragmentManager = getFragmentManager();
            Objects.requireNonNull(fragmentManager).beginTransaction().replace(R.id.include_indrawer, fragment).addToBackStack(null).commit();
            Bundle bundle = new Bundle();
            bundle.putString("fromSpinner", fromSpinner.getSelectedItem().toString());
            bundle.putString("toSpinner", toSpinner.getSelectedItem().toString());
            bundle.putInt("difficult", radioGroup.indexOfChild(Objects.requireNonNull(getActivity()).findViewById(radioGroup.getCheckedRadioButtonId())));
            fragment.setArguments(bundle);
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.train_no_words), Toast.LENGTH_SHORT).show();
        }
    }
}
