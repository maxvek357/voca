package com.example.maxvek.myapplication.utils;

import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.*;
import java.nio.channels.FileChannel;

public class FileUtils {
    public static void copyFile(@NonNull FileInputStream fromFile, @NonNull FileOutputStream toFile) throws IOException {
        FileChannel fromChannel = null;
        FileChannel toChannel = null;
        try {
            fromChannel = fromFile.getChannel();
            toChannel = toFile.getChannel();
            fromChannel.transferTo(0, fromChannel.size(), toChannel);
        } finally {
            try {
                if (fromChannel != null) {
                    fromChannel.close();
                }
            } finally {
                if (toChannel != null) {
                    toChannel.close();
                }
            }
        }
    }

    public static void copyDB(File src, String dst) throws IOException {
        try (InputStream in = new FileInputStream(src)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }

    public static void exportFile() {
        File checkFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "dictionary" + "/vocabulary.db");
        if (!checkFile.exists()) {
            File fdir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOCUMENTS), "dictionary");
            if (!fdir.mkdirs()) {
                Log.e("makedir", "Directory not created");
            }
            try {
                File file = new File(fdir, "vocabulary.db");
                FileOutputStream stream = new FileOutputStream(file);
                stream.write("".getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
