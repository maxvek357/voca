package com.example.maxvek.myapplication.utils;

import android.database.Cursor;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import com.example.maxvek.myapplication.models.StatInt;
import com.example.maxvek.myapplication.repository.DatabaseInstance;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static com.example.maxvek.myapplication.utils.DatabaseHelper.TABLE_STAT_ALL;
import static com.example.maxvek.myapplication.utils.DatabaseHelper.TABLE_STAT_WORD;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class DatabaseHelperTest {
    private DatabaseHelper db;

    @SuppressWarnings("deprecation")
    @Before
    public void setUp() throws Exception {
        db = new DatabaseHelper(getTargetContext());
        db.deleteAll("translate");
    }

    @After
    public void tearDown() throws Exception {
        db.close();
    }

    @Test
    public void insertData() {
        boolean result = db.insertData("uk", "en", "для", "for");
        Map<String, List<String>> hmapVoc = db.getAllForVocabulary("uk", "en");
        assertTrue(result);
        assertThat(hmapVoc.size(), is(1));
        assertTrue(hmapVoc.containsKey("для"));
    }

    @Test
    public void insertStatAll() {
        boolean result = db.insertStatAll(2, 3);
        assertTrue(result);
    }

    @Test
    public void insertStatWord() {
        db.insertStatWord("для", "uk", true);
        List<StatInt> statInts = db.getChartInfo(TABLE_STAT_WORD, "uk");
        assertNotNull(statInts);
    }

    @Test
    public void getChartInfo() {
        db.insertData("uk", "en", "для", "for");
        db.insertStatWord("для", "uk", true);
        List<StatInt> statInts = db.getChartInfo(TABLE_STAT_WORD, "uk");
        assertNotNull(statInts);
        assertThat(statInts.size(), is(5));
    }

    @Test
    public void getAllForVocabulary() {
        db.insertData("uk", "en", "для", "for");
        Map<String, List<String>> hmapVoc = db.getAllForVocabulary("uk", "en");
        assertNotNull(hmapVoc);
        assertThat(hmapVoc.size(), is(1));
        assertTrue(hmapVoc.containsKey("для"));
    }

    @Test
    public void getWordData() {
        db.insertData("uk", "en", "для", "for");
        Cursor cursor = db.getWordData("uk", "для");
        if (cursor.moveToFirst()){
            assertEquals(cursor.getString(cursor.getColumnIndex("uk")), "для");
        }
    }

    @Test
    public void getWordStat() {
        db.insertData("uk", "en", "для", "for");
        db.insertStatWord("для", "uk", true);
        StatInt statInt = db.getWordStat("для","uk");
        assertNotNull(statInt);
        assertEquals(statInt.word, "для");
    }


    @Test
    public void getHint() {
        db.insertData("uk", "en", "привіт", "hello");
        db.insertData("uk", "en", "проект", "project");
        List<String> list = new ArrayList<>(db.getHint("пр", "uk"));
        assertNotNull(list);
        assertThat(list.size(), is(1));
    }


    @Test
    public void deleteData() {
        db.insertData("uk", "en", "для", "for");
        db.deleteData("uk", "для");
        Cursor cursor = db.getWordData("uk", "для");
        assertThat(cursor.getCount(), is(0));
    }
}